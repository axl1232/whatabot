<?php
class Whatabot
{
    private $request;
    private $config;
    private $configFile;

    public function __construct()
    {
        $config = [];
        $this->request = $_REQUEST;
        $this->configFile = __DIR__ . "/config_{$this->request['auth']['domain']}.php";

        if (file_exists($this->configFile)) {
            $config = require $this->configFile;
        }

        $this->config = $config;
    }

    public function run()
    {
        switch ($this->request['event']) {
            case 'ONAPPINSTALL':
                $handlerBackUrl = ($_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http') . '://' . $_SERVER['SERVER_NAME'] . (in_array($_SERVER['SERVER_PORT'], array(80, 443)) ? '' : ":{$_SERVER['SERVER_PORT']}") . $_SERVER['SCRIPT_NAME'];

                $bot = $this->request(
                    'imbot.register',
                    [
                       'CODE' => 'Whatabot', // строковой идентификатор бота, уникальный в рамках вашего приложения (обяз.)
                       'TYPE' => 'B', // Тип бота, B - бот, ответы  поступают сразу, H - человек, ответы поступаю с задержкой от 2х до 10 секунд
                       'EVENT_MESSAGE_ADD' => $handlerBackUrl, // Ссылка на обработчик события отправки сообщения боту (обяз.)
                       'EVENT_WELCOME_MESSAGE' => $handlerBackUrl, // Ссылка на обработчик события открытия диалога с ботом или приглашения его в групповой чат (обяз.)
                       'EVENT_BOT_DELETE' => $handlerBackUrl, // Ссылка на обработчик события удаление бота со стороны клиента (обяз.)
                       'PROPERTIES' => [
                            'NAME' => 'Whatabot', // Имя бота (обязательное одно из полей NAME или LAST_NAME)
                            'LAST_NAME' => ' ', // Фамилия бота (обязательное одно из полей NAME или LAST_NAME)
                            'COLOR' => 'BROWN', // Цвет бота для мобильного приложения RED,  GREEN, MINT, LIGHT_BLUE, DARK_BLUE, PURPLE, AQUA, PINK, LIME, BROWN, AZURE, KHAKI, SAND, MARENGO, GRAY, GRAPHITE
                            'EMAIL' => 'main@whatasoft.net', // Емейл для связи
                            'PERSONAL_BIRTHDAY' => '2017-10-20', // День рождения в формате YYYY-mm-dd
                            'WORK_POSITION' => 'Верный друг', // Занимаемая должность, используется как описание бота
                            'PERSONAL_WWW' => 'https://whatasoft.net', // Ссылка на сайт
                            'PERSONAL_GENDER' => 'M', // Пол бота, допустимые значения M -  мужской, F - женский, пусто если не требуется указывать
                            'PERSONAL_PHOTO' => base64_encode(file_get_contents(__DIR__ . '/logo.png')), // Аватар бота - base64
                        ],
                    ],
                    $this->request['auth']
                );

                $ruletka = $this->request(
                    'imbot.command.register',
                    [
                        'BOT_ID' => $bot['result'],
                        'COMMAND' => 'рулетка',
                        'COMMON' => 'Y',
                        'HIDDEN' => 'N',
                        'EXTRANET_SUPPORT' => 'N',
                        'LANG' => [
                            ['LANGUAGE_ID' => 'ru', 'TITLE' => 'Сыграть с судьбой в рулетку', 'PARAMS' => ''],
                        ],
                        'EVENT_COMMAND_ADD' => $handlerBackUrl,
                    ],
                    $this->request['auth']
                );

                $vsratometr = $this->request(
                    'imbot.command.register',
                    [
                        'BOT_ID' => $bot['result'],
                        'COMMAND' => 'всратометр',
                        'COMMON' => 'Y',
                        'HIDDEN' => 'N',
                        'EXTRANET_SUPPORT' => 'N',
                        'LANG' => [
                            ['LANGUAGE_ID' => 'ru', 'TITLE' => 'Узнать всратость момента', 'PARAMS' => ''],
                        ],
                        'EVENT_COMMAND_ADD' => $handlerBackUrl,
                    ],
                    $this->request['auth']
                );

                $lyubov = $this->request(
                    'imbot.command.register',
                    [
                        'BOT_ID' => $bot['result'],
                        'COMMAND' => 'любовь',
                        'COMMON' => 'Y',
                        'HIDDEN' => 'N',
                        'EXTRANET_SUPPORT' => 'N',
                        'LANG' => [
                            ['LANGUAGE_ID' => 'ru', 'TITLE' => 'Узнать любовь к чему-либо', 'PARAMS' => ''],
                        ],
                        'EVENT_COMMAND_ADD' => $handlerBackUrl,
                    ],
                    $this->request['auth']
                );

                $config[$this->request['auth']['application_token']] = [
                   'BOT_ID' => $bot['result'],
                   'COMMAND_RULETKA' => $ruletka['result'],
                   'COMMAND_VSRATOMETR' => $vsratometr['result'],
                   'COMMAND_LYUBOV' => $lyubov['result'],
                   'LANGUAGE_ID' => $this->request['data']['LANGUAGE_ID'],
                ];

                $this->saveConfig($config);
                break;

            case 'ONIMBOTDELETE':
                $this->saveConfig([]);
                break;

            case 'ONIMBOTJOINCHAT':
                $this->request(
                    'imbot.message.add',
                    [
                        'DIALOG_ID' => $this->request['data']['PARAMS']['DIALOG_ID'],
                        'MESSAGE' => 'Удалите меня, пожалуйста.',
                        'ATTACH' => [],
                    ],
                    $this->request['auth']
                );
                break;

            case 'ONIMCOMMANDADD':
                foreach ($this->request['data']['COMMAND'] as $command) {
                    switch (strtolower($command['COMMAND'])) {
                        case 'всратометр':
                            $this->request(
                                'imbot.command.answer',
                                [
                                    'COMMAND_ID' => $command['COMMAND_ID'],
                                    'MESSAGE_ID' => $command['MESSAGE_ID'],
                                    'MESSAGE' => $this->processVsratometr($command['COMMAND_PARAMS']),
                                    'ATTACH' => [],
                                ],
                                $this->request['auth']
                            );
                            break;

                        case 'рулетка':
                            $this->request(
                                'imbot.command.answer',
                                [
                                    'COMMAND_ID' => $command['COMMAND_ID'],
                                    'MESSAGE_ID' => $command['MESSAGE_ID'],
                                    'MESSAGE' => $this->processRuletka($command['COMMAND_PARAMS']),
                                    'ATTACH' => [],
                                ],
                                $this->request['auth']
                            );
                            break;

                        case 'любовь':
                            $this->request(
                                'imbot.command.answer',
                                [
                                    'COMMAND_ID' => $command['COMMAND_ID'],
                                    'MESSAGE_ID' => $command['MESSAGE_ID'],
                                    'MESSAGE' => $this->processLyubov($command['COMMAND_PARAMS']),
                                    'ATTACH' => [],
                                ],
                                $this->request['auth']
                            );
                            break;
                    }
                }
                break;

            case 'ONIMBOTMESSAGEADD':
                $words = explode(' ', $this->request['data']['PARAMS']['MESSAGE']);
                $command = trim(array_shift($words));
                $params = implode(' ', $words);

                switch (strtolower($command)) {
                    case 'привет':
                        $this->request(
                            'imbot.message.add',
                            [
                                'DIALOG_ID' => $this->request['data']['PARAMS']['DIALOG_ID'],
                                'MESSAGE' => $this->processPrivet($params),
                                'ATTACH' => [],
                            ],
                            $this->request['auth']
                        );
                        break;

                    case 'всратометр':
                        $this->request(
                            'imbot.message.add',
                            [
                                'DIALOG_ID' => $this->request['data']['PARAMS']['DIALOG_ID'],
                                'MESSAGE' => $this->processVsratometr($params),
                                'ATTACH' => [],
                            ],
                            $this->request['auth']
                        );
                        break;

                    case 'рулетка':
                        $this->request(
                            'imbot.message.add',
                            [
                                'DIALOG_ID' => $this->request['data']['PARAMS']['DIALOG_ID'],
                                'MESSAGE' => $this->processRuletka($params),
                                'ATTACH' => [],
                            ],
                            $this->request['auth']
                        );
                        break;

                    case 'любовь':
                        $this->request(
                            'imbot.message.add',
                            [
                                'DIALOG_ID' => $this->request['data']['PARAMS']['DIALOG_ID'],
                                'MESSAGE' => $this->processLyubov($params),
                                'ATTACH' => [],
                            ],
                            $this->request['auth']
                        );
                        break;

                    default:
                        $this->request(
                            'imbot.message.add',
                            [
                                'DIALOG_ID' => $this->request['data']['PARAMS']['DIALOG_ID'],
                                'MESSAGE' => 'Отстань',
                                'ATTACH' => [],
                            ],
                            $this->request['auth']
                        );
                        break;
                }
                break;

            default:
                return;
        }
    }

    private function saveConfig($config)
    {
        file_put_contents($this->configFile, "<?php\nreturn " . var_export($config, true) . ";\n");
    }

    private function request($method, array $params = [], array $auth = [])
    {
        if (!isset($this->config[$this->request['auth']['application_token']])) {
            return false;
        }

        $curl = curl_init("https://{$auth['domain']}/rest/{$method}");

        curl_setopt_array(
            $curl,
            [
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POSTFIELDS => http_build_query(
                    array_merge($params, ['auth' => $auth['access_token']])
                ),
            ]
        );

        $result = curl_exec($curl);

        curl_close($curl);

        return json_decode($result, true);
    }

    private function log($data)
    {
        file_put_contents(
            __DIR__ . '/bot.log',
            var_export($data, true) . str_repeat(PHP_EOL, 2),
            FILE_APPEND
        );
    }

    private function processPrivet($params)
    {
        $values = ['Привет', 'Добрый день', 'Ага'];

        shuffle($values);

        return $this->request(
            'imbot.message.add',
            [
                'DIALOG_ID' => $this->request['data']['PARAMS']['DIALOG_ID'],
                'MESSAGE' => $values[0],
                'ATTACH' => [],
            ],
            $this->request['auth']
        );
    }

    private function processVsratometr($params)
    {
        $cacheTime = 30 * 60;
        $cacheId = "{$this->request['data']['PARAMS']['AUTHOR_ID']}_{$this->request['data']['PARAMS']['CHAT_ID']}";
        $cacheFile = __DIR__ . '/cache_vsratometr.php';
        $cache = [];

        if (file_exists($cacheFile)) {
            $cache = require $cacheFile;
        }

        $values = [
            0 => 'Поздравляю, вы мертвы :happy:',
            1 => 'Этого просто не может быть. Возможно наступил ядерный апокалипсис.',
            6 => 'Все хорошо. Но скоро все снова всрется :vzhuh:',
            15 => 'Жизнь похожа на сказку!',
            29 => 'Все лучше, чем могло бы быть.',
            37 => 'То ли всрато, то ли нет... Лол кек чебурек',
            50 => 'Обстановка лучше, чем обычно.',
            64 => 'Все как обычно. Изменений не предвидится.',
            73 => 'Все всрато. Но могло быть и хуже.',
            89 => 'Все сильно всратее, чем можно было бы себе представить.',
            94 => 'Винтовка - это праздник!',
            97 => 'Полный провал :sad:',
            105 => 'Зачем так жить?',
            112 => 'Да разве это жизнь?',
        ];

        if (
            !isset($cache[$this->request['auth']['application_token']][$cacheId])
            || time() - $cache[$this->request['auth']['application_token']][$cacheId]['time'] > $cacheTime
        ) {
            $hours = (int)date('H');
            $cache[$this->request['auth']['application_token']][$cacheId] = [
                'time' => time(),
                'percent' => mt_rand(
                    min(array_keys($values)) + ($hours < 14 ? 0 : $hours * 3),
                    max(array_keys($values))
                )
            ];

            file_put_contents($cacheFile, "<?php\nreturn " . var_export($cache, true) . ";\n");
        }

        $percent = $cache[$this->request['auth']['application_token']][$cacheId]['percent'];

        krsort($values);

        foreach ($values as $key => $value) {
            if ($percent >= $key) {
                return "Всрато на {$percent}%. {$value}";
            }
        }
    }

    private function processRuletka($params)
    {
        $isFemale = $this->request['data']['USER']['GENDER'] == 'F';
        $author = "[USER={$this->request['data']['USER']['ID']}]{$this->request['data']['USER']['NAME']}[/USER]";

        if (empty($params)) {
            if (rand(0, 5) < 2) {
                $results = [
                    "{$author} запускает рулетку и умирает. Сожалею.",
                    "{$author} запускает рулетку и " . ($isFemale ? 'её' : 'его') . ' голова разлетается словно тыква. Очень жаль.',
                    "После " . rand(3, 12) . " неудачных попыток запустить рулетку, {$author} с силой бросает её в угол и та в ответ вышибает " . ($isFemale ? 'ей' : 'ему') . ' мозги. Бывает.',
                    "{$author} стреляет себе в колено и умирает от кровотечения. " . ($isFemale ? 'Её' : 'Его') . ' даже не жаль.',
                    "Зря {$author} " . ($isFemale ? 'решила' : 'решил') . " сыграть с судьбой. Судьбу не обыграть. Вот и {$author} " . ($isFemale ? 'проиграла' : 'проиграл') . '.',
                    "{$author} запускает рулетку, но судьба не благосклонна к " . ($isFemale ? 'ней' : 'нему') . ". {$author} проигрывает. Се ля ви.",
                    "{$author} что-то подкручивает в рулетке и смело целит себе в голову. Раздается выстрел. Не умеешь - не берись.",
                ];
            } else {
                $oneMoreTime = '[send=/рулетка]Сыграем еще?[/send]';
                $results = [
                    "{$author} запускает рулетку и " . ($isFemale ? 'ей' : 'ему') . " везет. Поздравляю. {$oneMoreTime}",
                    "{$author} запускает рулетку и пуля проходит точно в отверстие, оставшееся от прошлой игры. Повезло. {$oneMoreTime}",
                    "{$author} запускает рулетку, но она не запускается. Везение - залог победы. {$oneMoreTime}",
                    "{$author} запускает рулетку и падает замертво. Но передумывает, встает и принимает поздравления с победой. {$oneMoreTime}",
                    "{$author} не " . ($isFemale ? 'смогла' : 'смог') . " запустить рулетку. Чистая победа. {$oneMoreTime}",
                    "{$author} стреляет в воздух и побеждает. " . ($isFemale ? 'Смекалистая' : 'Смекалистый') . ". {$oneMoreTime}",
                    "{$author} что-то подкручивает в рулетке и выигрывает. {$oneMoreTime}",
                ];
            }
        } else {
            if (trim(strtolower($params)) == 'я') {
                $results = [
                    "{$author} не выдерживает и самоубивается.",
                    "{$author} стреляет себе в голову.",
                    "{$author} стреляет себе в голову, но попадает в колено.",
                    "{$author} стреляет себе в голову, но не попадает.",
                    "{$author} продает рулетку, а на вырученные деньги до смерти упивается.",
                ];
            } else {
                $results = [
                    "{$author} убивает рулеткой {$params}.",
                    "{$author} запускает рулетку и {$params} мешком падает на холодный каменный пол. Все плачут и страдают.",
                    "{$author} запускает рулетку в {$params}, но та, облетев вокруг {$params}, убивает " . ($isFemale ? 'саму' : 'самого') . " {$author}. И поделом.",
                    "{$author} запускает рулетку в {$params}, но та выстреливает в воздухе в " . ($isFemale ? 'саму' : 'самого') . " {$author} и " . ($isFemale ? 'она' : 'он') . " умирает в муках. И поделом.",
                    "{$author} грозится обыграть в рулетку {$params}. Жалкое зрелище.",
                ];
            }
        }

        shuffle($results);

        return $results[0];
    }

    private function processLyubov($params)
    {
        $hash = "{$this->request['data']['USER']['ID']}_" . strtolower($params);
        $percent = 100 - hexdec(substr(md5($hash), 0, 8)) % 100;

        return "[USER={$this->request['data']['USER']['ID']}]{$this->request['data']['USER']['NAME']}[/USER] любит {$params} на {$percent}%";
    }
}
